package com.example.android.listviewdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import static java.util.Arrays.asList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView friendsListView = (ListView) findViewById(R.id.friendsListView);


        final ArrayList<String> theDudes = new ArrayList<String>(asList("James", "John", "Jack"));
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, theDudes);
        friendsListView.setAdapter(arrayAdapter);
        friendsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), "Hey " + theDudes.get(i), Toast.LENGTH_LONG).show();
            }
        });

//

//        ListView myListView = (ListView) findViewById(R.id.myListView);
//        final ArrayList<String> myFamily = new ArrayList<String>();
//
//        myFamily.add("James");
//        myFamily.add("John");
//        myFamily.add("Jaine");
//        myFamily.add("Jack");
//        myFamily.add("Jaimy");
//
//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, myFamily);
//        myListView.setAdapter(arrayAdapter);
//        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
////                adapterView.setVisibility(view.GONE);
//                Log.i("Tapped Button:", myFamily.get(i));
//            }
//        });
    }
}
